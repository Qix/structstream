package qix.structstream;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;

/**
 * Wraps a base translator to support
 * an array of that type
 * 
 * @author Qix
 */
class ArrayTranslator implements ITranslator<Object>
{
	private final Class<?> componentType;
	private final ITranslator<Object> baseTranslator;

	@SuppressWarnings("unchecked")
	public ArrayTranslator(
			Class<?> componentType, ITranslator<?> baseTranslator)
	{
		// Store
		this.componentType = componentType;
		this.baseTranslator = (ITranslator<Object>) baseTranslator;
	}

	@Override
	public void pack(DataOutputStream stream, Object data) throws IOException
	{
		// Get the length
		int length = Array.getLength(data);

		// Send the length
		stream.writeInt(length);

		// Write the values
		for (int i = 0; i < length; i++)
			this.baseTranslator.pack(stream, Array.get(data, i));
	}

	@Override
	public Object unpack(DataInputStream stream) throws IOException
	{
		// Read the length
		int length = stream.readInt();

		// Negative?
		if (length < 0)
			throw new IOException("encountered negative array length ("
					+ length + ")");

		// Create a new array
		Object array =
				java.lang.reflect.Array.newInstance(this.componentType, length);

		// Populate
		for (int i = 0; i < length; i++)
			Array.set(array, i, this.baseTranslator.unpack(stream));

		// Return
		return array;
	}
}
