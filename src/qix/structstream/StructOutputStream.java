package qix.structstream;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

/**
 * Wraps an output stream and allows
 * the sending of specially annotated
 * objects
 * 
 * @author Qix
 */
public class StructOutputStream
{
	private final DataOutputStream stream;

	public StructOutputStream(
			OutputStream stream)
	{
		// Store
		this.stream = new DataOutputStream(stream);
	}

	/**
	 * Writes an annotated object to the stream
	 * 
	 * @param o
	 *            The object to write
	 * @return The number of bytes written
	 * @throws IOException
	 *             Thrown if an error occurs while sending the object, either
	 *             when packing the data or generating translators for
	 *             the class' fields.
	 */
	@SuppressWarnings("unchecked")
	public int write(Object o) throws IOException
	{
		// Get initial count
		int initialCount = stream.size();

		// Get map of translators from cache
		Map<java.lang.reflect.Field, ITranslator<?>> translators =
				TranslatorCache.getTranslations(o.getClass());

		// Iterate fields and translations
		for (Map.Entry<java.lang.reflect.Field, ITranslator<?>> translation : translators
				.entrySet())
		{
			// Get accessibility
			final boolean accessible = translation.getKey().isAccessible();

			try
			{
				// Set accessible
				if (!accessible)
					translation.getKey().setAccessible(true);

				// Read the value
				Object value;
				try
				{
					value = translation.getKey().get(o);
				} catch (IllegalArgumentException | IllegalAccessException e)
				{
					// Re-throw
					throw new IOException(e);
				}

				// Pack the value
				try
				{
					((ITranslator<Object>) translation.getValue()).pack(
							this.stream, (Object) value);
				} catch (IOException e)
				{
					// Better describe where we're at
					throw new IOException(String.format(
							"Could not pack field %s.%s <%s>: %s", translation
									.getKey().getDeclaringClass().getName(),
							translation.getKey().getName(), translation
									.getKey().getType().getName(),
							e.getMessage()), e);
				}
			} finally
			{
				// Restore accessibility
				// We do a compare to avoid issues with
				// exceptions when the application KNOWS
				// it won't be dealing with any security issues
				// and is executing within a sandbox.
				if (translation.getKey().isAccessible() != accessible)
					translation.getKey().setAccessible(accessible);
			}
		}

		// Subtract initial count from ending count
		// and return
		return stream.size() - initialCount;
	}

	/**
	 * @return The underlying data output stream associated
	 *         with this StructOutputStream
	 */
	public DataOutputStream getDataStream()
	{
		// Return
		return this.stream;
	}
}
