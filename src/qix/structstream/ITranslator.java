package qix.structstream;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Defines a translator for a specific type
 * that puts/gets data from a data stream
 * and converts it to an object.
 * 
 * @author Qix
 */
public interface ITranslator<E>
{
	/**
	 * Packs (sends) data into a stream
	 * 
	 * @param stream
	 *            The stream to operate on
	 * @param data
	 *            The data to send
	 * @throws IOException
	 *             Thrown when I/O issues arise
	 */
	public void pack(DataOutputStream stream, E data) throws IOException;

	/**
	 * Unpacks (receives) data from a stream
	 * 
	 * @param stream
	 *            The stream to operate on
	 * @return A new object of the generic type
	 * @throws IOException
	 *             Thrown when I/O issues arise
	 */
	public E unpack(DataInputStream stream) throws IOException;
}
