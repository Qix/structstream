package qix.structstream.test;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

import qix.structstream.StructInputStream;
import qix.structstream.StructOutputStream;

/**
 * Tests the structstream system
 * 
 * @author Qix
 */
public class StructStreamTester
{
	public static void main(String[] args) throws IOException
	{
		System.out.println("Struct1: " + test(new Struct1(true)));
		System.out.println("Struct2: " + test(new Struct2(true)));
		System.out.println("Struct3: " + test(new Struct3(true)));
	}

	public static String test(Object obj) throws IOException
	{
		// Create streams
		PipedOutputStream outputStream = new PipedOutputStream();
		PipedInputStream inputStream = new PipedInputStream(outputStream);

		// Create struct streams
		StructOutputStream structOut = new StructOutputStream(outputStream);
		StructInputStream structIn = new StructInputStream(inputStream);

		// Output the data
		structOut.write(obj);

		// Read the data
		Object obj2 = structIn.read(obj.getClass());

		// Compare
		return String.format("%s (%b)", obj2.toString(), obj.equals(obj2));
	}
}
