package qix.structstream.test;

import qix.structstream.Field;

public class Struct2
{
	@Field(0)
	public String val1 = "";

	@Field(1)
	public String val2 = "This is another string";

	public Struct2()
	{

	}

	public Struct2(
			boolean first)
	{
		this.val1 = "This is a string!";
		this.val2 = "This is a modified string.";
	}

	@Override
	public String toString()
	{
		return "Struct2 [val1=" + val1 + ", val2=" + val2 + "]";
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((val1 == null) ? 0 : val1.hashCode());
		result = prime * result + ((val2 == null) ? 0 : val2.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Struct2 other = (Struct2) obj;
		if (val1 == null)
		{
			if (other.val1 != null)
				return false;
		} else if (!val1.equals(other.val1))
			return false;
		if (val2 == null)
		{
			if (other.val2 != null)
				return false;
		} else if (!val2.equals(other.val2))
			return false;
		return true;
	}

}
