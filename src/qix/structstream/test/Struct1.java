package qix.structstream.test;

import qix.structstream.Field;

public class Struct1
{
	@Field(0)
	public int val1 = -1;

	@Field(1)
	public int val2 = -1;

	@Field(2)
	public float val3 = -1.0f;

	public int unused = 5;

	public Struct1()
	{

	}

	public Struct1(
			boolean first)
	{
		this.val1 = 5;
		this.val2 = 12734;
		this.val3 = -9183.474f;
	}

	@Override
	public String toString()
	{
		return "Struct1 [val1=" + val1 + ", val2=" + val2 + ", val3=" + val3
				+ ", unused=" + unused + "]";
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + unused;
		result = prime * result + val1;
		result = prime * result + val2;
		result = prime * result + Float.floatToIntBits(val3);
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Struct1 other = (Struct1) obj;
		if (unused != other.unused)
			return false;
		if (val1 != other.val1)
			return false;
		if (val2 != other.val2)
			return false;
		if (Float.floatToIntBits(val3) != Float.floatToIntBits(other.val3))
			return false;
		return true;
	}

}
