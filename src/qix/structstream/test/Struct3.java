package qix.structstream.test;

import java.util.Arrays;

import qix.structstream.Field;

public class Struct3
{
	@Field(0)
	private int[] arr1 = null;
	@Field(1)
	private Float[] arr2 = new Float[] { 0.1f, 2.0f };
	@Field(2)
	String[] arr3 = new String[] {};

	public Struct3()
	{

	}

	public Struct3(
			boolean first)
	{
		this.arr1 = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 1000 };
		this.arr2 = new Float[] { 9.0f, 8.0f, -7777.0f };
		this.arr3 = new String[] { "Hello", "there", "qix!" };
	}

	@Override
	public String toString()
	{
		return "Struct3 [arr1=" + Arrays.toString(arr1) + ", arr2="
				+ Arrays.toString(arr2) + ", arr3=" + Arrays.toString(arr3)
				+ "]";
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(arr1);
		result = prime * result + Arrays.hashCode(arr2);
		result = prime * result + Arrays.hashCode(arr3);
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Struct3 other = (Struct3) obj;
		if (!Arrays.equals(arr1, other.arr1))
			return false;
		if (!Arrays.equals(arr2, other.arr2))
			return false;
		if (!Arrays.equals(arr3, other.arr3))
			return false;
		return true;
	}

}
