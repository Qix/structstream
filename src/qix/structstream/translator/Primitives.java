package qix.structstream.translator;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import qix.structstream.ITranslator;

/**
 * Implements translators for all primitives
 * and their Object counterparts
 * 
 * @author Qix
 */
public final class Primitives
{
	private Primitives()
	{
	}

	/**
	 * Translator for a 4 byte integer
	 */
	public static final ITranslator<Integer> INTEGER =
			new ITranslator<Integer>()
			{
				@Override
				public Integer unpack(DataInputStream stream) throws IOException
				{
					// Return
					return stream.readInt();
				}

				@Override
				public void pack(DataOutputStream stream, Integer data) throws IOException
				{
					// Send
					stream.writeInt(data);
				}
			};

	/**
	 * Translator for a 1 byte boolean
	 */
	public static final ITranslator<Boolean> BOOLEAN =
			new ITranslator<Boolean>()
			{
				@Override
				public Boolean unpack(DataInputStream stream) throws IOException
				{
					// Return
					return stream.readBoolean();
				}

				@Override
				public void pack(DataOutputStream stream, Boolean data) throws IOException
				{
					// Send
					stream.writeBoolean(data);
				}
			};

	/**
	 * Translator for a 2 byte character
	 */
	public static final ITranslator<Character> CHARACTER =
			new ITranslator<Character>()
			{
				@Override
				public Character unpack(DataInputStream stream) throws IOException
				{
					// Return
					return stream.readChar();
				}

				@Override
				public void pack(DataOutputStream stream, Character data) throws IOException
				{
					// Send
					stream.writeChar(data);
				}
			};

	/**
	 * Translator for a 2 byte short
	 */
	public static final ITranslator<Short> SHORT = new ITranslator<Short>()
	{
		@Override
		public Short unpack(DataInputStream stream) throws IOException
		{
			// Return
			return stream.readShort();
		}

		@Override
		public void pack(DataOutputStream stream, Short data) throws IOException
		{
			// Send
			stream.writeShort(data);
		}
	};

	/**
	 * Translator for 1 byte
	 */
	public static final ITranslator<Byte> BYTE = new ITranslator<Byte>()
	{
		@Override
		public Byte unpack(DataInputStream stream) throws IOException
		{
			// Return
			return stream.readByte();
		}

		@Override
		public void pack(DataOutputStream stream, Byte data) throws IOException
		{
			// Send
			stream.writeByte(data);
		}
	};

	/**
	 * Translator for a string
	 * Converts to UTF-8 and then sends/recieves
	 */
	public static final ITranslator<String> STRING = new ITranslator<String>()
	{
		@Override
		public String unpack(DataInputStream stream) throws IOException
		{
			// Return
			return stream.readUTF();
		}

		@Override
		public void pack(DataOutputStream stream, String data) throws IOException
		{
			// Send
			stream.writeUTF(data);
		}
	};

	/**
	 * Translator for an 8 byte double
	 */
	public static final ITranslator<Double> DOUBLE = new ITranslator<Double>()
	{
		@Override
		public Double unpack(DataInputStream stream) throws IOException
		{
			// Return
			return stream.readDouble();
		}

		@Override
		public void pack(DataOutputStream stream, Double data) throws IOException
		{
			// Send
			stream.writeDouble(data);
		}
	};

	/**
	 * Translator for a 4 byte float
	 */
	public static final ITranslator<Float> FLOAT = new ITranslator<Float>()
	{
		@Override
		public Float unpack(DataInputStream stream) throws IOException
		{
			// Return
			return stream.readFloat();
		}

		@Override
		public void pack(DataOutputStream stream, Float data) throws IOException
		{
			// Send
			stream.writeFloat(data);
		}
	};

	/**
	 * Translator for an 8 byte long
	 */
	public static final ITranslator<Long> LONG = new ITranslator<Long>()
	{
		@Override
		public Long unpack(DataInputStream stream) throws IOException
		{
			// Return
			return stream.readLong();
		}

		@Override
		public void pack(DataOutputStream stream, Long data) throws IOException
		{
			// Send
			stream.writeLong(data);
		}
	};

}
