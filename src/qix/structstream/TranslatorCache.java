package qix.structstream;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Traverses and caches class hierarchies
 * in order to get a full, ordered list
 * of annotated field parameters
 * 
 * @author Qix
 */
final class TranslatorCache
{
	private TranslatorCache()
	{
	}

	/**
	 * The cache of translator->field mappings for classes
	 */
	private static final Map<Class<?>, Map<java.lang.reflect.Field, ITranslator<?>>> CACHE =
			new HashMap<>();

	private static final Map<Class<?>, ITranslator<?>> ARRAY_CACHE =
			new HashMap<>();

	/**
	 * Gets translations for a class or generates them if
	 * they don't exist
	 * 
	 * @param clazz
	 *            The class for which to retrieve translations
	 * @return A translation map of fields to translators, in order
	 * @throws IOException
	 *             Thrown if an exception occurs when generating translations
	 */
	public static Map<java.lang.reflect.Field, ITranslator<?>> getTranslations(	Class<?> clazz) throws IOException
	{
		// Get the translations map
		Map<java.lang.reflect.Field, ITranslator<?>> translations =
				TranslatorCache.CACHE.get(clazz);

		// Null?
		if (translations == null)
			return TranslatorCache.generateFor(clazz);
		else
			return translations;
	}

	/**
	 * Generates a map of field->translations for a class and
	 * caches them
	 * 
	 * @param clazz
	 *            The class for which to generate translations
	 * @return A map of field translations, in order
	 * @throws IOException
	 *             Thrown if an exception occurs when generating translations,
	 *             namely if a field is of a type that isn't registered
	 *             with a translator
	 */
	private static <E> Map<java.lang.reflect.Field, ITranslator<?>> generateFor(Class<E> clazz) throws IOException
	{
		// Create map
		Map<java.lang.reflect.Field, ITranslator<?>> translations =
				new LinkedHashMap<>();

		// Get field list
		Collection<java.lang.reflect.Field> fields =
				TranslatorCache.getOrderedFieldList(clazz);

		// Iterate fields and check to see if they
		// have corresponding translators
		for (final java.lang.reflect.Field field : fields)
		{
			// Get type
			Class<?> fieldType = field.getType();

			// Is there a translator for it?
			ITranslator<?> translator =
					Translators.getTranslatorForClass(fieldType);
			if (translator == null)
			{
				// Is it an array?
				if (fieldType.isArray())
				{
					// Get base
					final Class<?> componentType = fieldType.getComponentType();

					// Is this a 2+ dimension array?
					if (componentType.isArray())
						throw new IOException(
								String.format(
										"multiple-dimension arrays not supported: %s.%s",
										field.getDeclaringClass().getName(),
										field.getName()));

					// Do we have a cached array wrapper for it?
					translator = TranslatorCache.ARRAY_CACHE.get(componentType);
					if (translator == null)
					{
						// Get base translator
						final ITranslator<?> baseTranslator =
								Translators
										.getTranslatorForClass(componentType);

						// Is it unsupported?
						if (baseTranslator == null)
							throw new IOException(String.format(
									"type not supported: %s.%s", field
											.getDeclaringClass().getName(),
									field.getName()));

						// Create translator
						// We can assume so many things here because
						// we've already weeded out extensive edge cases
						// (i.e. multiple-dimension arrays) and have
						// already type checked all of our translators.
						// This allows us to work on the data as if they're
						// plain Objects (it's as if they were wildcards).
						translator =
								new ArrayTranslator(componentType,
										baseTranslator);

						// Cache this wrapper
						TranslatorCache.ARRAY_CACHE.put(componentType,
								translator);
					}
				}
			}

			// Add to the map
			translations.put(field, translator);
		}

		// Cache the translations
		TranslatorCache.CACHE.put(clazz, translations);

		// Return
		return translations;
	}

	/**
	 * Iterates a class' hierachy to find all of the fields that
	 * are marked with {@link Field} and returns them, in order
	 * 
	 * @param clazz
	 *            The class to iterate
	 * @return An ordered list of fields
	 * @throws IOException
	 *             Thrown when an reflection exception occurs or if fields have
	 *             duplicate offsets
	 */
	private static <E> Collection<java.lang.reflect.Field> getOrderedFieldList(	Class<E> clazz) throws IOException
	{
		// Create initial field mapping
		Map<Integer, java.lang.reflect.Field> fieldMap = new LinkedHashMap<>();

		// Iterate the class' hierarchy
		Class<? super E> current = clazz;
		do
		{
			// Iterate fields
			for (java.lang.reflect.Field field : current.getDeclaredFields())
			{
				// Does the field have a field annotation?
				Field fieldAnnot = field.getAnnotation(Field.class);
				if (fieldAnnot == null)
					continue;

				// Add to the map
				java.lang.reflect.Field oldField =
						fieldMap.put(fieldAnnot.value(), field);

				// Did we overwrite something?
				if (oldField != null)
					throw new IOException(
							String.format(
									"field %s.%s overwrites %s.%s with struct order %d",
									oldField.getDeclaringClass().getName(),
									oldField.getName(), current.getName(),
									field.getName(), fieldAnnot.value()));
			}
		} while ((current = clazz.getSuperclass()) != Object.class);

		// Get map entries
		List<Map.Entry<Integer, java.lang.reflect.Field>> entries =
				new LinkedList<Map.Entry<Integer, java.lang.reflect.Field>>(
						fieldMap.entrySet());

		// Sort the entries
		Collections.sort(entries,
				new Comparator<Map.Entry<Integer, java.lang.reflect.Field>>()
				{
					@Override
					public int compare(	Entry<Integer, java.lang.reflect.Field> a,
										Entry<Integer, java.lang.reflect.Field> b)
					{
						// Return
						return a.getKey().compareTo(b.getKey());
					}
				});

		// Create a new, sorted map
		Map<Integer, java.lang.reflect.Field> sortedMap =
				new LinkedHashMap<Integer, java.lang.reflect.Field>();
		for (Map.Entry<Integer, java.lang.reflect.Field> entry : entries)
			sortedMap.put(entry.getKey(), entry.getValue());

		// Return
		return sortedMap.values();
	}
}
