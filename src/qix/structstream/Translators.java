package qix.structstream;

import java.util.HashMap;
import java.util.Map;

import qix.structstream.translator.Primitives;

/**
 * Lists the translators
 * 
 * @author Qix
 */
public final class Translators
{
	static
	{
		// Create translators array
		Translators.TRANSLATORS = new HashMap<>();

		// Register primitives
		Translators.registerTranslator(Integer.class, Primitives.INTEGER);
		Translators.registerTranslator(Short.class, Primitives.SHORT);
		Translators.registerTranslator(Double.class, Primitives.DOUBLE);
		Translators.registerTranslator(Long.class, Primitives.LONG);
		Translators.registerTranslator(Float.class, Primitives.FLOAT);
		Translators.registerTranslator(Byte.class, Primitives.BYTE);
		Translators.registerTranslator(Character.class, Primitives.CHARACTER);
		Translators.registerTranslator(String.class, Primitives.STRING);
		Translators.registerTranslator(Boolean.class, Primitives.BOOLEAN);
		Translators.registerTranslator(int.class, Primitives.INTEGER);
		Translators.registerTranslator(short.class, Primitives.SHORT);
		Translators.registerTranslator(double.class, Primitives.DOUBLE);
		Translators.registerTranslator(long.class, Primitives.LONG);
		Translators.registerTranslator(float.class, Primitives.FLOAT);
		Translators.registerTranslator(byte.class, Primitives.BYTE);
		Translators.registerTranslator(char.class, Primitives.CHARACTER);
		Translators.registerTranslator(boolean.class, Primitives.BOOLEAN);
	}

	private Translators()
	{
	}

	/**
	 * Holds a list of translators and maps them to their classes
	 */
	private static Map<Class<?>, ITranslator<?>> TRANSLATORS;

	/**
	 * Gets a translator for a specified class
	 * 
	 * @param clazz
	 *            The class to lookup
	 * @return The translator that can handle a particular class, or null if the
	 *         class isn't registered
	 */
	@SuppressWarnings("unchecked")
	static <E> ITranslator<E> getTranslatorForClass(Class<? extends E> clazz)
	{
		// Return
		return (ITranslator<E>) Translators.TRANSLATORS.get(clazz);
	}

	/**
	 * Registers (overwrites) a class and a corresponding translator for packet
	 * serialization
	 * 
	 * @param clazz
	 *            The class to register
	 * @param translator
	 *            The translator that handles the conversion of data
	 */
	public static <E> void registerTranslator(Class<E> clazz,
												ITranslator<E> translator)
	{
		// Put
		Translators.TRANSLATORS.put(clazz, translator);
	}

	/**
	 * Removes a class from the translators list
	 * 
	 * @param clazz
	 *            The class to un-register
	 */
	public static void unregisterTranslator(Class<?> clazz)
	{
		// Remove
		Translators.TRANSLATORS.remove(clazz);
	}
}
