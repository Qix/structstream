package qix.structstream;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * Wraps an input stream and allows
 * the receiving of specially annotated
 * objects
 * 
 * @author Qix
 */
public class StructInputStream
{
	private final DataInputStream stream;

	public StructInputStream(
			InputStream stream)
	{
		// Store
		this.stream = new DataInputStream(stream);
	}

	/**
	 * Retrieves an annotated object from the stream
	 * The class must have a default constructor
	 * 
	 * @important THIS DOES NOT PERFORM TYPECHECKING.
	 *            You MUST know the type of contents in the stream
	 *            before receiving!!
	 * @param clazz
	 *            The class of the object that is to be received
	 * @return An object of the specified class, populated with data
	 *         from the stream
	 * @throws IOException
	 *             Thrown if an error occurs while receiving the object, either
	 *             when unpacking the data or generating translators for
	 *             the class' fields.
	 */
	@SuppressWarnings("unchecked")
	public <E> E read(Class<E> clazz) throws IOException
	{
		// Create a new instance of the class
		Object o;
		try
		{
			o = clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e)
		{
			// Re-throw
			throw new IOException(e);
		}

		// Populate
		this.populate(o);

		// Return
		return (E) o;
	}

	/**
	 * Populates an annotated object with data from the stream
	 * 
	 * @important THIS DOES NOT PERFORM TYPECHECKING.
	 *            You MUST know the type of contents in the stream
	 *            before receiving!!
	 * @param o
	 *            The object to populate
	 * @throws IOException
	 *             Thrown if an error occurs while receiving the object, either
	 *             when unpacking the data or generating translators for
	 *             the class' fields.
	 */
	@SuppressWarnings("unchecked")
	public void populate(Object o) throws IOException
	{
		// Get map of translators from cache
		Map<java.lang.reflect.Field, ITranslator<?>> translators =
				TranslatorCache.getTranslations(o.getClass());

		// Iterate fields and translations
		for (Map.Entry<java.lang.reflect.Field, ITranslator<?>> translation : translators
				.entrySet())
		{
			// Get accessibility
			final boolean accessible = translation.getKey().isAccessible();

			try
			{
				// Set accessible
				if (!accessible)
					translation.getKey().setAccessible(true);

				// Unpack the value
				Object value;
				try
				{
					value =
							((ITranslator<Object>) translation.getValue())
									.unpack(this.stream);
				} catch (IOException e)
				{
					// Better describe where we're at
					throw new IOException(String.format(
							"Could not unpack data for field %s.%s <%s>: %s",
							translation.getKey().getDeclaringClass().getName(),
							translation.getKey().getName(), translation
									.getKey().getType().getName(),
							e.getMessage()), e);
				}

				// Write the value
				try
				{
					translation.getKey().set(o, value);
				} catch (IllegalArgumentException | IllegalAccessException e)
				{
					// Re-throw
					throw new IOException(e);
				}
			} finally
			{
				// Restore accessibility
				// We do a compare to avoid issues with
				// exceptions when the application KNOWS
				// it won't be dealing with any security issues
				// and is executing within a sandbox.
				if (translation.getKey().isAccessible() != accessible)
					translation.getKey().setAccessible(accessible);
			}
		}
	}

	/**
	 * @return The underlying data input stream associated
	 *         with this StructInputStream
	 */
	public DataInputStream getDataStream()
	{
		// Return
		return this.stream;
	}
}
